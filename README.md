# Article Manager

## Libraries Used
This project relies on the following third-party libraries:

- **Material-UI**: The various data tables in the application are built with the pre-designed `DataGrid` component provided by the Material-UI library.
- **Axios**: The requests made by the application to the backend are handled using the Axios library.
- **Yup**: The forms within the application are validated with the YUP library.
- **Bootstrap**: The styling of the application primarily utilizes Bootstrap classes.

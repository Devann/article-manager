const actionsTypes = {
  SET_NOTIFICATION: "SET_NOTIFICATION",
  SET_TAGS: "SET_TAGS",
  SET_ARTICLES: "SET_ARTICLES",
  ADD_ARTICLE: "ADD_ARTICLE",
  EDIT_ARTICLE: "EDIT_ARTICLE",
  DELETE_ARTICLE: "DELETE_ARTICLE",
};

export default actionsTypes;

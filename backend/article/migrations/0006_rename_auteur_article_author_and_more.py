# Generated by Django 5.0.1 on 2024-04-20 10:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0005_alter_article_summary'),
    ]

    operations = [
        migrations.RenameField(
            model_name='article',
            old_name='auteur',
            new_name='author',
        ),
        migrations.RenameField(
            model_name='article',
            old_name='favoris',
            new_name='favorite',
        ),
        migrations.RenameField(
            model_name='article',
            old_name='nom',
            new_name='name',
        ),
        migrations.AddField(
            model_name='article',
            name='reread',
            field=models.BooleanField(default=False),
        ),
    ]

# Generated by Django 5.0.1 on 2024-04-21 15:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0007_rename_reread_article_read_again'),
    ]

    operations = [
        migrations.RenameField(
            model_name='article',
            old_name='url_article',
            new_name='url',
        ),
        migrations.RemoveField(
            model_name='article',
            name='url_site',
        ),
    ]

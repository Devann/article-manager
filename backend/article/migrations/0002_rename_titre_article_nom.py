# Generated by Django 5.0.1 on 2024-01-20 09:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='article',
            old_name='titre',
            new_name='nom',
        ),
    ]

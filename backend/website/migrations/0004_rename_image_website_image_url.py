# Generated by Django 5.0.1 on 2024-01-11 14:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0003_alter_website_image'),
    ]

    operations = [
        migrations.RenameField(
            model_name='website',
            old_name='image',
            new_name='image_url',
        ),
    ]
